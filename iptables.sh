# With iptables the first rule that matches a packet will be applied.
# policies - ACCEPT/DROP
# targets - ACCEPT/DROP/REJECT/LOG/RETURN

# flush iptables
iptables -F

# povolit odchozi spojeni na port 80 ze vsech imaginarnich stroju za mym routerem
iptables -A FORWARD -p tcp --dport 80

# povolit odchozi spojeni na porty 0-1024 do internetu
iptables -A OUTPUT -p tcp --dport 0:1024

# user www-data nemuze inicializovat odchozi spojeni mimo loopback
iptables -A OUTPUT -m owner --uid-owner www-data -j REJECT

#filtrovani podle stavu
#    NEW - paket vytváří nové spojení nebo se vztahuje ke spojení, kde dosud neproběhla obousměrná komunikace
#    ESTABLISHED - paket se vztahuje ke spojení, kde probíhá obousměrná komunikace (tedy již k vytvořenému spojení)
#    RELATED - paket vytváří nové spojení, ale vztahuje se k některému z existujících (např. u FTP)
#    INVALID - paket se nevztahuje k žádnému známému spojení (obvykle je to paket, který tu nemá co dělat, a proto je dobré ho rovnou zahazovat)
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# display current iptables configuration
iptables -L
