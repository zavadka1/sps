# flush iptables
iptables -F

# allow SSH from all sources
iptables -I INPUT -p tcp --dport 22 -j ACCEPT

# allow loopback
iptables -I INPUT -i lo -j ACCEPT

# chains - INPUT/FORWARD/OUTPUT

# change policy: -P <chain> <policy>
iptables -P INPUT ACCEPT

# povolit prichozi spojeni na port 80 a 443
iptables -A INPUT -p tcp --dport 80
iptables -A INPUT -p tcp --dport 443 -j ACCEPT

# FTP
iptables -A INPUT -p tcp --dport 21 -j ACCEPT
iptables -A INPUT -p tcp --dport 20 -j ACCEPT
# magic for FTP passive mode, requires the ip_conntrack_ftp module
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# DNS
iptables -A INPUT -p tcp --dport 53 -j ACCEPT
iptables -A INPUT -p udp --dport 53 -j ACCEPT

# Block specific IP (insert on 3rd place)
iptables -I INPUT 3 -s 192.168.123.0/24 -j DROP

# display current iptables configuration
iptables -L
