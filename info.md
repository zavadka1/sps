# networking

netstat:
`netstat -tulpn`

open and listen:
`nc -l 5000`

connect:
`nc <address> 5000`

receive data:
`nc -l 5000 > data.txt`

send data:
`nc <address> 5000 < input.txt `
or `nc <address> 5000 < input.txt `

# iptables

- http://bencane.com/2012/09/17/iptables-linux-firewall-rules-for-a-basic-web-server/
- https://l.facebook.com/l.php?u=http%3A%2F%2Fwww.abclinuxu.cz%2Fblog%2FDebian_Lenny%2F2009%2F10%2Fzakladni-konfigurace-linux-firewallu-pomoci-iptables%3Ffbclid%3DIwAR0mjQA1mIWoHR_G6ZQnhP75yd0RwISR3zBf72PERpF3f92QQpfPOHoVkNQ&h=AT3Hw5TCRi6b2HpO1p7ymf7kwSFFTwIwOpKlYaLcb8WVzafYHkQMQhfaBP75P1dFqTS2hDUYtLT4fnzf1wHBTVwaB52O6Y05BQi4rK1ax3ZSjj6G32eOwt5qITA
- https://gist.github.com/mcastelino/c38e71eb0809d1427a6650d843c42ac2

# bind9

- debug with **dig** `dig A @<dns-server> <address> [+short]`
- restart `sudo service bind9 restart`
- https://www-uxsup.csx.cam.ac.uk/pub/doc/redhat/redhat7.3/rhl-rg-en-7.3/s1-bind-configuration.html

# ssl

- https://www.linux.com/learn/creating-self-signed-ssl-certificates-apache-linux

create request: `openssl req -new > new.ssl.csr`

```
sudo openssl rsa -in privkey.pem -out new.cert.key
sudo openssl x509 -in new.cert.csr -out new.cert.cert -req -signkey new.cert.key -days NNN
sudo cp new.cert.cert /etc/ssl/certs/server.crt
sudo cp new.cert.key /etc/ssl/private/server.key
```

Apache:

```
<VirtualHost *:443>
    ServerAdmin
 This e-mail address is being protected from spambots. You need JavaScript enabled to view it

    ServerName mydomain.net
    ServerAlias www.mydomain.net
    DocumentRoot /srv/www/mydomain.net/public_html/

    ErrorLog /srv/www/mydomain.net/logs/error.log
    CustomLog /srv/www/mydomain.net/logs/access.log combined

    SSLEngine on
    SSLOptions +StrictRequire
    SSLCertificateFile /etc/ssl/certs/server.crt
    SSLCertificateKeyFile /etc/ssl/private/server.key
</VirtualHost>
```

# Icinga 2

- https://www.linode.com/docs/uptime/monitoring/install-icinga2-monitoring-on-debian-9/
