#sudo openssl req -new > new.ssl.csr
sudo openssl rsa -in privkey.pem -out new.cert.key
sudo openssl x509 -in new.ssl.csr -out new.cert.cert -req -signkey new.cert.key -days 100
sudo cp new.cert.cert /etc/ssl/certs/server.crt
sudo cp new.cert.key /etc/ssl/private/server.key
